#!/afs/cats.ucsc.edu/courses/cmps112-wm/usr/racket/bin/mzscheme -qr
;; $ - - 1421365, epoels: sbi.scm,v 1.3 2016-09-23 18:23:20-07 - - $
;;
;; NAME
;;    sbi.scm - silly basic interpreter
;;
;; SYNOPSIS
;;    sbi.scm filename.sbir
;;
;; DESCRIPTION
;;    The file mentioned in argv[1] is read and assumed to be an SBIR
;;    program, which is the executed.  Currently it is only printed.
;;

(define *stderr* (current-error-port))

(define *run-file*
    (let-values
        (((dirpath basepath root?)
            (split-path (find-system-path 'run-file))))
        (path->string basepath))
)

(define (die list)
    (for-each (lambda (item) (display item *stderr*)) list)
    (newline *stderr*)
    (exit 1)
)

(define (usage-exit)
    (die `("Usage: " ,*run-file* " filename"))
)

(define (readlist-from-inputfile filename)
    (let ((inputfile (open-input-file filename)))
         (if (not (input-port? inputfile))
             (die `(,*run-file* ": " ,filename ": open failed"))
             (let ((program (read inputfile)))
                  (close-input-port inputfile)
                         program))))

(define *function-table* (make-hash))
(define (function-get key)
        (hash-ref *function-table* key))
(define (function-put! key value)
        (hash-set! *function-table* key value))

(for-each
    (lambda (pair)
            (function-put! (car pair) (cadr pair)))
    `(

        (log10_2 0.301029995663981195213738894724493026768189881)
        (sqrt_2  1.414213562373095048801688724209698078569671875)
        (e       2.718281828459045235360287471352662497757247093)
        (pi      3.141592653589793238462643383279502884197169399)
        (div     ,(lambda (x y) (floor (/ x y))))
        (log10   ,(lambda (x) (/ (log x) (log 10.0))))
        (mod     ,(lambda (x y) (- x (* (div x y) y))))
        (quot    ,(lambda (x y) (truncate (/ x y))))
        (rem     ,(lambda (x y) (- x (* (quot x y) y))))
        (+       ,+)
        (^       ,expt)
        (ceil    ,ceiling)
        (exp     ,exp)
        (floor   ,floor)
        (log     ,log)
        (sqrt    ,sqrt)
        (-       ,-)
        (*       ,*)
        (quot    ,(lambda (x y) (truncate (/ x y))))
        (rem     ,(lambda (x y) (-x (* (quot x y) y )))) 
        (log     ,(lambda (number) (log-helper number)))
        (/       ,(lambda numbers (divide-helper numbers)))
        (mod     ,(lambda (w u) (- x (* (div x y) y))))
        (<>      ,(lambda numbers (not (apply = numbers))))
        (abs     ,abs)
        (acos    ,acos)
        (asin    ,asin)
        (atan    ,atan)
        (cos     ,cos)
        (round   ,round)
        (sin     ,sin)
        (tan     ,sin)
        (trunc   ,truncate)
        (<       ,<)
        (>       ,>)
        (<=      ,<=)
        (>=      ,>=)
        (=       ,=)
        (inputcount ,-1)
        (dim     ,(lambda (array) (dim-helper array)))
        (let     ,(lambda (key value) (let-helper key value)))
        (goto    ,(lambda (key) (hash-ref *label-table* key #f)))
        (if      ,(lambda (bool label) (if-helper bool label)))
        (print   ,(lambda strings (print-helper strings)))
        (input   ,(lambda inputs 
                     (hash-set! *variable-table* 'inputcount 0) 
                        (for-each input-helper inputs)
                  )
        )

    )
)

(define (log-helper number)
   (if (= 0 number)
      (log (+ number 0.0))
      (log number)
   )
)

(define *label-table* (make-hash))
(define (label-get key)
        (hash-ref *label-table* key))
(define (label-put! key value)
        (hash-set! *label-table* key value))

(define (label-grabber program)
   (cond 
      ((empty? (cdar program)) #f)
      ((symbol? (cadar program)) 
         (label-put! (cadar program) program)
      )
      (else #f)
   )
)

(define *variable-table* (make-hash))
(define (variable-get key)
        (hash-ref *variable-table* key))
(define (variable-put! key value)
        (hash-set! *variable-table* key value))

(for-each
    (lambda (pair)
            (variable-put! (car pair) (cadr pair)))

    '(

        (e      2.718281828459045235360287471352662497757247093)
        (pi     3.141592653589793238462643383279502884197169399)

     )
)

;(define (write-program-by-line filename program)
   ; (printf "==================================================~n")
   ; (printf "~a: ~s~n" *run-file* filename)
   ; (printf "==================================================~n")
   ; (printf "(~n")
   ; (map (lambda (line) (printf "~s~n" line)) program)
   ; (printf ")~n"))
;)

; -- a function to evaluate when there is a division by zero -- ;
(define (divide-by-zero-case number) 
   (map (lambda (x) (+ 0.0 x)) number)
)

; -- this helps determin if special commands need to be ran -- ;
(define check-equivilency (lambda (phrase)
  ; (printf "Phrase: ~s~n" phrase)
   (if (hash-has-key? *function-table* phrase)
      (cond 
         ((eqv? (function-get 'let) (function-get phrase)) #t)
         ((eqv? (function-get 'input) (function-get phrase)) #t)
         (else #f)
      )
   #f
   ))
)

(define (len program)
   (define (len.. program acc)
      (cond 
         ((empty? program) acc)
         (else (len.. (rest program) (+ acc 1)))
      )
   )
   (len.. program 0)
)

; -- a helper function to divide numbers given by the program -- ;
(define (divide-helper numbers)
   (cond
      ((list? numbers)
         (if (= (car (rest numbers)) 0)
            (apply / (divide-by-zero-case numbers))
            (apply / numbers)
         )
      )
      (else
         (if (= numbers 0) 
            (apply / (+ 0.0 numbers))
            (apply / numbers)
         )
      )
   )
)

; -- a helper function to run the DIM command -- ;
(define (dim-helper array)
   ;(printf "Dim-Helper: Start  ~n")
   (hash-set! *variable-table* (car array) 
      (make-vector (+ (cadr array) 1))
   ) 
   ;(printf "Dim-Helper: Finished ~n")
)

; -- a helper function to run the LET command -- ;
(define (let-helper key value)
   (if (list? key)
      (vector-set! (car key) (cadr key) value)
      (hash-set! *variable-table* key 
         (hash-ref *variable-table* value value))
   )
)

; -- a helper function to run the IF command -- ;
(define (if-helper bool label)
   (if (not (false? bool))
      (hash-ref *label-table* label #f) #f
   )
)

; -- a helper function to run the PRINT command -- ;
(define (print-helper input) 
   (for-each display input)
   (printf "~n")
)

; -- a helper function to run the INPUT command -- ;
(define input-helper (lambda (line)
   (define input (read)) 
      (cond 
         ((eof-object? input) 
            (hash-set! *variable-table* 'inputcount (- 1))
         )
         ((number? input) 
            (hash-set! *variable-table* line input) 
            (hash-set! *variable-table* 'inputcount 
               (+ (hash-ref *variable-table* 'inputcount) 1))
         )
         (else #f)
      )
   )
)

; -- This is used to subsitute expressions and symbols from -- ;
; -- the appropiate tables when a line requires it -- ;
(define (expression-help phrase)
   (cond 
      ((number? phrase) phrase)
      ((symbol? phrase) phrase)
      ((string? phrase) phrase)
      ((pair? phrase)   
         (cond 
            ((vector? (hash-ref *variable-table* (car phrase) #f))
               (list (hash-ref *variable-table* (car phrase)) 
                  (debug (cadr phrase))
               )
            )
            ((check-equivilency (car phrase)) 
               (apply 
                  (hash-ref *function-table* (car phrase)) 
                  (map expression-help (cdr phrase))
               )
            )
            (else 
               (if (hash-ref *function-table* (car phrase) #f) 
                  (apply 
                     (hash-ref *function-table* (car phrase))
                     (map debug (cdr phrase))
                  ) 
                  (map debug phrase)
               ) 
            )
         )
      )
      (else phrase)
   )
)

;debugs the line
(define (debug phrase)
   (cond 
      ((number? phrase) phrase)
      ((symbol? phrase) 
         (hash-ref *variable-table* phrase 
            (hash-ref *function-table* phrase phrase)
         )
      )
      ((string? phrase) phrase)
      ((pair? phrase)
        ; (printf "Debug Phrase: ~s~n" phrase)   
         (cond 
            ((vector? (hash-ref *variable-table* (car phrase) #f)) 
               (vector-ref (hash-ref *variable-table* (car phrase) #f) 
                   (hash-ref *variable-table* 
                      (cadr phrase) (cadr phrase)
                   )
               )
            )
            ((check-equivilency (car phrase)) 
               (apply 
                  (hash-ref *function-table* (car phrase)) 
                  (map expression-help (cdr phrase))
               )
            )
            (else 
               (if 
                  (hash-ref *function-table* (car phrase) #f) 
                     (apply 
                        (hash-ref *function-table* (car phrase))
                        (map debug (cdr phrase))
                     )
                    (map debug phrase)
               )
            )
         )
      )
      (else phrase)
   )
)

;; Check to see if line is empty. If not, evaluate it.
(define (evaluate-line line)
   (cond 
      ((empty? (cdar line)) #f)
      ((symbol? (cadar line)) 
         (if 
            (< (len (car line)) 3) #f (debug (caddar line))
         )
      )
      (else 
         (debug (cadar line))
      )
   )
) 

; -- A function used to parse each line from the program using 
; the appropiate mode, (label) or not label -- ;
(define (parse-lines function line)
   (define (parse-lines.. line program)
      (define template (function line))
         (cond 
            ((list? template) (parse-lines.. template program))
            ((empty? (cdr line)) program)
            (else (parse-lines.. (cdr line) program))
         )
      )
   (parse-lines.. line '())
)

(define *stderr* (current-error-port))

(define *run-file*
    (let-values
        (((dirpath basepath root?)
            (split-path (find-system-path 'run-file))))
        (path->string basepath))
)

;function gicen
(define (die list)
    (for-each (lambda (item) (display item *stderr*)) list)
    (newline *stderr*)
    (exit 1)
)

;function given to die
(define (usage-exit)
    (die `("Usage: " ,*run-file* " filename"))
)

;function given to read in the file
(define (readlist-from-inputfile filename)
    (let ((inputfile (open-input-file filename)))
         (if (not (input-port? inputfile))
             (die `(,*run-file* ": " ,filename ": open failed"))
             (let ((program (read inputfile)))
                  (close-input-port inputfile)
                         program))))

;(define (write-program-by-line filename program)
    ;;(printf "==================================================~n")
    ;;(printf "~a: ~s~n" *run-file* filename)
    ;;(printf "==================================================~n")
    ;;(printf "(~n")
    ;;(map (lambda (line) (evaluate-line line)) program)
    ;(printf "~n")
;)

(define (main arglist)
    (if (or (null? arglist) (not (null? (cdr arglist))))
        (usage-exit)
        (let* ((sbprogfile (car arglist))
               (program (readlist-from-inputfile sbprogfile)))
               (parse-lines label-grabber program) ;creates label table
               ;runs the rest of the program
               (parse-lines evaluate-line program)
        )
    )
)

(main (vector->list (current-command-line-arguments)))
