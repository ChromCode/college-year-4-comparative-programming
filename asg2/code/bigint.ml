(* $Id: bigint.ml,v 1.5 2014-11-11 15:06:24-08 - - $ *)

open Printf

module Bigint = struct

    type sign     = Pos | Neg
    type bigint   = Bigint of sign * int list
    let  radix    = 10
    let  radixlen =  1

    let length    = List.length 
    let car       = List.hd
    let cdr       = List.tl
    let map       = List.map
    let reverse   = List.rev
    let strcat    = String.concat
    let strlen    = String.length
    let strsub    = String.sub
    let zero      = Bigint (Pos, [])

    let charlist_of_string str = 
        let last = strlen str - 1
        in  let rec charlist pos result =
            if pos < 0
            then result
            else charlist (pos - 1) (str.[pos] :: result)
        in  charlist last []

    let bigint_of_string str =
        let len = strlen str
        in  let to_intlist first =
                let substr = strsub str first (len - first) in
                let digit char = int_of_char char - int_of_char '0' in
                map digit (reverse (charlist_of_string substr))
            in  if   len = 0
                then zero
                else if   str.[0] = '_'
                     then Bigint (Neg, to_intlist 1)
                     else Bigint (Pos, to_intlist 0)

    let string_of_bigint (Bigint (sign, value)) =
        match value with
        | []    -> "0"
        | value -> let reversed = reverse value
                   in  strcat ""
                       ((if sign = Pos then "" else "-") ::
                        (map string_of_int reversed))

    (* ----- Begin Helper functions ----- *)

    (* Compare Helper Function *)
    (* Returns a comparison value in the same way as does strcmp in C. *)
    let rec cmp val1 val2 = match (val1, val2) with
        | [], []                    -> 0
        | list1, []                 -> 1
        | [], list2                 -> 2
        | car1::cdr1, car2::cdr2    -> 
            if length (car1::cdr1) > length (car2::cdr2) then 1
            else if length (car1::cdr1) < length (car2::cdr2) then 2
            else if cdr1 = [] && not (cdr2 = []) then 2
            else if not (cdr1 = []) && cdr2 = [] then 1
            else if car1 > car2 then 1
            else if car2 > car1 then 2
            else cmp cdr1 cdr2

    (* Deletes Leading Zeros from a Number *)
    let deleteLeadingZeros list = 
        let rec deleteLeadingZeros' list' = match list' with
            | []                -> []
            | [0]               -> []
            | car::cdr          ->
                let cdr' = deleteLeadingZeros' cdr
                in match car, cdr' with
                    | 0, []        -> []
                    | car, cdr'    -> car::cdr'

        in deleteLeadingZeros' list

    (* ----- End Helper Functions ----- *)

    let rec add' list1 list2 carry = match (list1, list2, carry) with
        | list1, [], 0       -> list1
        | [], list2, 0       -> list2
        | list1, [], carry   -> add' list1 [carry] 0
        | [], list2, carry   -> add' [carry] list2 0
        | car1::cdr1, car2::cdr2, carry ->
          let sum = car1 + car2 + carry
          in  sum mod radix :: add' cdr1 cdr2 (sum / radix)

    let rec sub' list1 list2 carry = match (list1, list2, carry) with
        | list1, [], 0       -> list1
        | [], list2, 0       -> list2
        | list1, [], carry   -> sub' list1 [carry] 0
        | [], list2, carry   -> sub' [carry] list2 0
        | car1::cdr1, car2::cdr2, carry ->
            if(car1 - carry) < car2 then let diff  
                = (10 + (car1 - car2 - carry)) in diff
                mod radix :: sub' cdr1 cdr2 1
            else let diff
                = car1 - car2 - carry in diff
                mod radix :: sub' cdr1 cdr2 0

    (* Subtraction of BigInts and Negatives *)
    let sub (Bigint (neg1, val1)) (Bigint (neg2, val2)) = 
        if neg1 = neg2
        then let comparison = (cmp (reverse val1) (reverse val2)) in 
            if comparison = 1 
                then Bigint (neg1, deleteLeadingZeros (sub' val1 val2 0))
            else if comparison = 0 then zero 
            else Bigint 
                (
                    (if neg1 = Pos then Neg else Pos), 
                    deleteLeadingZeros (sub' val2 val1 0)
                )
        else Bigint (neg1, deleteLeadingZeros (add' val1 val2 0))

    let add (Bigint (neg1, value1)) (Bigint (neg2, value2)) =
        if neg1 = neg2
        then Bigint (neg1, add' value1 value2 0)
        else let comparison = (cmp (reverse value1) (reverse value2)) in 
        if comparison = 1 
            then Bigint (neg1, sub' value1 value2 0) 
        else if comparison = 0 
            then zero 
        else 
            Bigint (neg2, sub' value2 value1 0)

    (* Recursive function to add value to itself *)  
    (* as long as powerof2 is not zero*)
    let rec incrementExp value powerOf2 = 
        if powerOf2 = 0 
            then value
        else incrementExp (add' value value 0) (powerOf2 - 1)

    (* The Subtraction Portion of Egyptian Multiplication*)
    let rec mulsub' list1 list2 powerOf2 result = match (list1, list2, powerOf2, result) with
        | list1, list2, -1, _                -> result
        | [0], list2, _, _                   -> result
        | list1, [0], _, _                   -> result
        (*| list1, list2, powerOf2, result     -> 
          if (cmp (reverse list1) (reverse (incrementExp [1] powerOf2))) = 2 then mul'' list1 list2 (powerOf2 - 1) result
          else mulsub' (deleteLeadingZeros (sub' list1 (incrementExp [1] powerOf2) 0)) list2 (powerOf2 - 1) (add' result (incrementExp list2 powerOf2) 0)*)

    (* The addition portion of Egyptian Multiplication*)
    let rec mul' list1 list2 powerOf2 = match (list1, list2, powerOf2) with
        | [], list2, _               -> 0
        | list1, [], _               -> 0
        | list1, list2, powerOf2     -> 
            if(cmp(reverse list1) (reverse(incrementExp [1] powerOf2))) = 2
            then mulsub' list1 list2 (sub' powerOf2 1 0) [0]
            else mul' list1 list2 (powerOf2 + 1) 
    
    (* Mult Function to handle BigInts*)
    let mul (Bigint (neg1, val1)) (Bigint (neg2, val2)) = 
        if neg1 = neg2
        then Bigint(Pos, mul' val1, val2, 0)
        else Bigint(Neg, mul' val2, val3, 0)

    let div = add

    let rem = add

    let pow = add

end

